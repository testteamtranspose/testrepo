package com.dplesa.spring.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dplesa.spring.rest.model.Rule;

@RestController
public class ExampleController {
	
	@RequestMapping("/example")
    public List<Rule> example() {
		
		List<Rule> ruleList = new ArrayList<Rule>();
		
		Rule rule = new Rule();
		rule.setId(1);
		rule.setText("Must be big!");
		ruleList.add(rule);
		
		Rule rule2 = new Rule();
		rule2.setId(2);
		rule2.setText("Must be exciting!");
		ruleList.add(rule2);
		
		Rule rule3 = new Rule();
		rule3.setId(3);
		rule3.setText("Must be user friendly!");
		ruleList.add(rule3);
		
		return ruleList;
    }
	
}
